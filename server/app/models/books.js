/*jslint node: true*/
"use strict";

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    modelName,
    schemaName;

/**
 * Creates a new mongoose schema.
 * -----
 * Instructions, hints and questions
 * - Instruction: create a Schema with the following properties
 *   - title
 *   - author
 *   - description
 *   - modificationDate
 * - The properties are defined as follow:
 *   - title, author and description are strings
 *   - modificationDate is a date
 *   - a title of the book is unique
 *   - title and author are required
 *   - the default value for date is now
 *
 * - Question: What are the differences between a 'Schema Type' and a JSON object? Use the references to motivate your answer.
 * Antwoord: Het Schema Type bepaald hoe het JSON object eruit moet zien. Het JSON object een string representatie van een of meerdere objecten en/of arrays
 * - Question: What is the function of the 'collection' property?
 * Antwoord: Het is de naam van de MongoDb collection waarin CRUD operaties worden uitgevoerd voor een Mongoose model
 * - Question: Suppose you have a model for 'Person'. What would be the default collection name?
 * Antwoord: Persons. Er wordt automatisch meervoud van gemaakt.
 * @class Schema/Book
 * @returns Schema object
 * @see http://www.json.org/
 * @see http://mongoosejs.com/docs/schematypes.html
 * @see http://mongoosejs.com/docs/guide.html#collection
 */
schemaName = new Schema(
    {
        title: {type: String, required: true, unique: true},
        author: {type: String, required: true},
        description: {type: String},
        modificationDate: {type: Date, "default": Date.now}
    },
    {collection: 'books'}
);

/**
 * Custom validator
 * -------
 * Instructions, hints and questions.
 *
 * In Mongoose you can define custom validators.
 * If the value does not fit the the definition, an error is returned.

 * - Instruction: Add a validator for title. A title must have a length of at least 8 characters.
 * - Question: There are four locations of validations, each for a specific type of validation. Give for each validation an example and describe why that location is the best location for that specific type of validation.
 *   1. Database (technical constraints, primary key)
 *      Database vervuiling voorkomen
 *   1. Schema (simple business rules)
 *      Het schema bepaald hoe het object eruit moet zien, dit is de beste manier om datatypes te definiëren
 *   1. Application (complex business rules)
 *      In de applicatie kan je complexe code stoppen en dus complexe business rules implementeren
 *   1. Client-side (form validation)
 *      offline validatie van gegevens die in een form worden ingevuld
 * @class Validator/Book/title
 * @returns true or false. In case of ```false```, a message 'Invalid title' is returned as well.
 * @see http://mongoosejs.com/docs/validation.html
 */
schemaName.path('title').validate(function (val) {
    return (val !== undefined && val !== null && val.length >= 8);
}, 'Invalid title');

/**
 * Instructions, hints and questions.
 * - Instruction: Create a model for the defined schema.
 * - Question: What are the differences between a 'Model' and a 'Schema Type'? Use the references to motivate your answer.
 * Antwoord: Een model is een object met data erin. Schema types worden gebruikt voor data validatie, casting, defaults voor queries en andere algemene opties voor strings en numbers in een model.
 * @class Model/Book
 * @see http://mongoosejs.com/docs/models.html
 */
modelName = "Book";
module.exports = mongoose.model(modelName, schemaName);